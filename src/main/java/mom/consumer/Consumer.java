package mom.consumer;

import mom.consumer.messaging.ConsumerReceiveProtocol;
import mom.producer.Producer;
import mom.queue.MessageQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Consumer {
    private final Logger LOGGER = LogManager.getLogger(Producer.class.getName());
    private final Socket CLIENT_SOCKET;
    private final MessageQueue messageQueue = new MessageQueue();
    private PrintWriter outputMessageStream = null;
    private boolean available;
    private String publishQueue;
    private String subscribeQueue;
    public Consumer(Socket clientSocket) {
        this.CLIENT_SOCKET = clientSocket;
        this.available = true;

        try {
            this.outputMessageStream = new PrintWriter(CLIENT_SOCKET.getOutputStream(), true);
        } catch (IOException exception) {
            LOGGER.error("The output stream cannot be created due to the following error (" + exception.getMessage() + ")");
        }

        initializeInputStream();
    }

    public MessageQueue getMessageQueue() {
        return messageQueue;
    }

    public String getPublishQueue() {
        return publishQueue;
    }

    public void setPublishQueue(String publishQueue) {
        this.publishQueue = publishQueue;
    }

    public String getSubscribeQueue() {
        return subscribeQueue;
    }

    public void setSubscribeQueue(String subscribeQueue) {
        this.subscribeQueue = subscribeQueue;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public PrintWriter getOutputMessageStream() {
        return outputMessageStream;
    }

    public void initializeInputStream() {
        try {
            BufferedReader inputMessageStream = new BufferedReader(new InputStreamReader(CLIENT_SOCKET.getInputStream()));
            new ConsumerReceiveProtocol(inputMessageStream, this).start();
        } catch (IOException exception) {
            LOGGER.error("The output stream cannot be created due to the following error (" + exception.getMessage() + ")");
        }
    }
}

package mom.consumer;

import mom.producer.Producer;
import mom.queue.MessageQueue;
import mom.queue.QueueCreator;
import mom.queue.QueueMessageSender;
import mom.server.MiddlewareServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class ConsumerActions {
    private final static Logger LOGGER = LogManager.getLogger(ConsumerActions.class.getName());
    private final static Producer PRODUCER = MiddlewareServer.producer;

    public static void onDisconnect(Consumer consumer) {
        LOGGER.warn("Consumer client just disconnected");
        List<Consumer> consumers = MiddlewareServer.consumers;
        consumers.remove(consumer);
    }

    public static void onDeclareQueue(Consumer consumer, String queueName, String type) {
        LOGGER.warn("A consumer is trying to create a queue...");
        QueueCreator.declareQueue(queueName);

        if (type.equals("PUBLISH")) {
            consumer.setPublishQueue(queueName);
        } else if (type.equals("SUBSCRIBE")) {
            consumer.setSubscribeQueue(queueName);

            MessageQueue messageQueue = QueueCreator.messageQueueHashtable.get(queueName);
            for (String message : messageQueue.getMessages()) {
                consumer.getMessageQueue().enqueueMessage(message);
            }

            LOGGER.info("A new consumer is subscribed to queue=" + queueName + " in mode=" + type);

            new QueueMessageSender(consumer.getOutputMessageStream(), consumer.getMessageQueue().getMessages()).start();
        }
    }

    public static void onReceiveMessage(String queueName, String message) {
        LOGGER.info("Receiving message from Producer...");
        MessageQueue messageQueue = QueueCreator.messageQueueHashtable.get(queueName);
        messageQueue.enqueueMessage(message);
        LOGGER.info("The message will be send to queue=" + queueName);

        if (PRODUCER.getSubscribeQueue().equals(queueName)) {
            PRODUCER.getMessageQueue().enqueueMessage(message);
        }
    }
}

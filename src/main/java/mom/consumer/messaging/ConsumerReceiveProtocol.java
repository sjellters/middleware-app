package mom.consumer.messaging;

import mom.consumer.Consumer;
import mom.consumer.ConsumerActions;
import mom.message.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;

public class ConsumerReceiveProtocol extends Thread {
    private final Logger LOGGER = LogManager.getLogger(ConsumerReceiveProtocol.class.getName());
    private final BufferedReader INPUT_MESSAGE_READER;
    private final Consumer CONSUMER;

    public ConsumerReceiveProtocol(BufferedReader inputMessageReader, Consumer consumer) {
        this.INPUT_MESSAGE_READER = inputMessageReader;
        this.CONSUMER = consumer;
    }

    @Override
    public void run() {
        super.run();
        String message;

        boolean isActive = true;

        while (isActive) {
            try {
                if (((message = INPUT_MESSAGE_READER.readLine()) != null)) {
                    Message parsedMessage = Message.parse(message);

                    switch (parsedMessage.getAction()) {
                        case "DISCONNECT":
                            ConsumerActions.onDisconnect(CONSUMER);
                            isActive = false;
                            break;
                        case "DECLARE_QUEUE":
                            ConsumerActions.onDeclareQueue(CONSUMER, parsedMessage.getQueueName(), parsedMessage.getMessage());
                            break;
                        case "MESSAGE":
                            ConsumerActions.onReceiveMessage(parsedMessage.getQueueName(), parsedMessage.getMessage());
                            break;
                        default:
                            break;
                    }
                } else {
                    break;
                }
            } catch (IOException exception) {
                LOGGER.error("An error occurred during input streaming due to the following exception ( " + exception.getMessage() + " )");
            }
        }
    }
}

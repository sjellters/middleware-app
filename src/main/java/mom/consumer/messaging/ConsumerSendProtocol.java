package mom.consumer.messaging;

import java.io.PrintWriter;

public class ConsumerSendProtocol {
    public static void sendMessage(PrintWriter outputMessageStream, String message) {
        outputMessageStream.println(message);
    }
}

package mom.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
    private final static Logger logger = LogManager.getLogger(PropertiesLoader.class.getName());

    public static String getProperty(String key) {
        Properties properties = new Properties();

        try {
            InputStream inputStream = PropertiesLoader.class.getClassLoader().getResourceAsStream("application.properties");
            properties.load(inputStream);
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (NullPointerException e) {
            logger.error("Properties file does not exist, generating a null exception.");
        }

        return properties.getProperty(key);
    }
}

package mom;

import mom.server.MiddlewareServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MiddlewareApplication {
    private static final Logger LOGGER = LogManager.getLogger(MiddlewareApplication.class.getName());

    public static void main(String[] args) {
        MiddlewareServer middlewareServer = new MiddlewareServer();
        LOGGER.info("MOM Server is starting...");
        middlewareServer.start();
    }
}

package mom.queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.util.Queue;

public class QueueMessageSender extends Thread {
    private final Logger LOGGER = LogManager.getLogger(QueueMessageSender.class.getName());
    public PrintWriter outputMessageStream;
    public Queue<String> messageQueue;

    public QueueMessageSender(PrintWriter outputMessageStream, Queue<String> messageQueue) {
        this.outputMessageStream = outputMessageStream;
        this.messageQueue = messageQueue;
    }

    @Override
    public void run() {
        super.run();

        while (true) {
            String message;
            while ((message = messageQueue.poll()) == null) {
                Thread.onSpinWait();
            }

            outputMessageStream.println(message);
            LOGGER.info("Message sent");
        }
    }
}

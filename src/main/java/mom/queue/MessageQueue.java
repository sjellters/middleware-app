package mom.queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;

public class MessageQueue {
    private final Logger LOGGER = LogManager.getLogger(MessageQueue.class.getName());
    private final Queue<String> messages;

    public MessageQueue() {
        this.messages = new LinkedList<>();
    }

    public Queue<String> getMessages() {
        return messages;
    }

    public void enqueueMessage(String message) {
        messages.offer(message);
    }

    public String dequeueMessage() {
        String nextMessage = messages.poll();
        if (nextMessage == null) {
            LOGGER.warn("There is no more message on this queue");
        }
        return nextMessage;
    }
}

package mom.queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Hashtable;

public class QueueCreator {
    private final static Logger LOGGER = LogManager.getLogger(QueueCreator.class.getName());

    public static Hashtable<String, MessageQueue> messageQueueHashtable = new Hashtable<>();

    public static boolean checkIfExist(String queueName) {
        return messageQueueHashtable.containsKey(queueName);
    }

    public static void declareQueue(String queueName) {
        if (!checkIfExist(queueName)) {
            messageQueueHashtable.put(queueName, new MessageQueue());
            LOGGER.info("A new queue was created ( name=" + queueName + " )");
        } else {
            LOGGER.warn("A client tried to create a new queue but it exists( using queue=" + queueName + " )");
        }
    }
}

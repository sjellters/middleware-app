package mom.server.connection;

import mom.consumer.Consumer;
import mom.server.MiddlewareServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class ConsumerConnection extends Thread {
    final Logger LOGGER = LogManager.getLogger(ConsumerConnection.class.getName());

    final ServerSocket SERVER_SOCKET = MiddlewareServer.serverSocketConsumer;
    final List<Consumer> CONSUMERS = MiddlewareServer.consumers;

    public ConsumerConnection() {
    }

    @Override
    public void run() {
        super.run();

        while (true) {
            try {
                final Socket CLIENT_SOCKET = SERVER_SOCKET.accept();
                final Consumer CONSUMER = new Consumer(CLIENT_SOCKET);
                CONSUMERS.add(CONSUMER);
                LOGGER.info("A consumer client just connected to the port " + SERVER_SOCKET.getLocalPort() + " [current_number_of_consumers=" + CONSUMERS.size() + "]");
            } catch (IOException exception) {
                LOGGER.error("And exception occurred while a consumer client tried to connect ( " + exception.getMessage() + " )");
            }
        }
    }
}

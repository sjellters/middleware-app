package mom.server.connection;

import mom.producer.Producer;
import mom.server.MiddlewareServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

public class ProducerConnection extends Thread {
    private final Logger LOGGER = LogManager.getLogger(ProducerConnection.class.getName());

    private final ServerSocket SERVER_SOCKET = MiddlewareServer.serverSocketProducer;
    private final Producer PRODUCER = MiddlewareServer.producer;
    private final AtomicBoolean PRODUCER_IS_CONNECTED = new AtomicBoolean();

    public ProducerConnection() {
        this.PRODUCER_IS_CONNECTED.set(false);
    }

    @Override
    public void run() {
        super.run();

        while (true) {
            while (PRODUCER_IS_CONNECTED.get()) {
                Thread.onSpinWait();
            }

            try {
                LOGGER.warn("Waiting for producer client connection...");
                final Socket CLIENT_SOCKET = SERVER_SOCKET.accept();
                PRODUCER_IS_CONNECTED.set(true);
                PRODUCER.setClientSocket(CLIENT_SOCKET);
                PRODUCER.setActive(PRODUCER_IS_CONNECTED);
                PRODUCER.initializeInputStream();
                LOGGER.info("A producer client just connected to the port " + SERVER_SOCKET.getLocalPort());
            } catch (IOException exception) {
                LOGGER.error("A producer client cannot connect due to the following error ( " + exception.getMessage() + " )");
            }
        }
    }
}

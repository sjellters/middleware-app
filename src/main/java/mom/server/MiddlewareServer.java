package mom.server;

import mom.consumer.Consumer;
import mom.producer.Producer;
import mom.server.connection.ConsumerConnection;
import mom.server.connection.ProducerConnection;
import mom.utils.PropertiesLoader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public class MiddlewareServer {
    public final static Producer producer = new Producer();
    public final static List<Consumer> consumers = new ArrayList<>();
    public static ServerSocket serverSocketProducer = null;
    public static ServerSocket serverSocketConsumer = null;

    final int PRODUCER_PORT = Integer.parseInt(PropertiesLoader.getProperty("producer.port"));
    final int CONSUMER_PORT = Integer.parseInt(PropertiesLoader.getProperty("consumer.port"));
    final Logger LOGGER = LogManager.getLogger(MiddlewareServer.class.getName());

    public void start() {
        try {
            serverSocketProducer = new ServerSocket(PRODUCER_PORT);
            new ProducerConnection().start();
        } catch (IOException exception) {
            LOGGER.error("The producer server socket cannot be initialized due to the following error ( " + exception.getMessage() + " )");
        }

        try {
            serverSocketConsumer = new ServerSocket(CONSUMER_PORT);
            new ConsumerConnection().start();
        } catch (IOException exception) {
            LOGGER.error("The consumer server socket cannot be initialized due to the following error ( " + exception.getMessage() + " )");
        }
    }
}

package mom.producer.messaging;

import mom.message.Message;
import mom.producer.ProducerActions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

public class ProducerReceiveProtocol extends Thread {
    private final Logger LOGGER = LogManager.getLogger(ProducerReceiveProtocol.class.getName());
    private final BufferedReader INPUT_MESSAGE_READER;
    private final AtomicBoolean PRODUCER_IS_ACTIVE;

    public ProducerReceiveProtocol(BufferedReader inputMessageReader, AtomicBoolean producerIsActive) {
        this.INPUT_MESSAGE_READER = inputMessageReader;
        this.PRODUCER_IS_ACTIVE = producerIsActive;
    }

    @Override
    public void run() {
        super.run();
        String message;

        while (true) {
            try {
                if (((message = INPUT_MESSAGE_READER.readLine()) != null)) {
                    Message parsedMessage = Message.parse(message);

                    switch (parsedMessage.getAction()) {
                        case "DISCONNECT":
                            ProducerActions.onDisconnect(PRODUCER_IS_ACTIVE);
                            break;
                        case "DECLARE_QUEUE":
                            ProducerActions.onDeclareQueue(parsedMessage.getQueueName(), parsedMessage.getMessage());
                            break;
                        case "MESSAGE":
                            ProducerActions.onReceiveMessage(parsedMessage);
                            break;
                        default:
                            break;
                    }
                } else {
                    break;
                }
            } catch (IOException exception) {
                LOGGER.error("An error occurred during input streaming due to the following exception ( " + exception.getMessage() + " )");
            }
        }
    }
}

package mom.producer.messaging;

import mom.producer.Producer;
import mom.server.MiddlewareServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProducerSendProtocol {
    private final static Logger LOGGER = LogManager.getLogger(ProducerSendProtocol.class.getName());
    private final static Producer PRODUCER = MiddlewareServer.producer;

    public static void sendMessage(String message) {
        PRODUCER.getOutputMessageStream().println(message);
        LOGGER.info("Sending message to producer...");
    }
}

package mom.producer;

import mom.producer.messaging.ProducerReceiveProtocol;
import mom.queue.MessageQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;

public class Producer {
    private final Logger LOGGER = LogManager.getLogger(Producer.class.getName());
    private final MessageQueue messageQueue = new MessageQueue();
    private Socket clientSocket;
    private AtomicBoolean active;
    private String publishQueue;
    private String subscribeQueue;
    private PrintWriter outputMessageStream;

    public Producer() {
    }

    public MessageQueue getMessageQueue() {
        return messageQueue;
    }

    public PrintWriter getOutputMessageStream() {
        return outputMessageStream;
    }

    public String getPublishQueue() {
        return publishQueue;
    }

    public void setPublishQueue(String publishQueue) {
        this.publishQueue = publishQueue;
    }

    public String getSubscribeQueue() {
        return subscribeQueue;
    }

    public void setSubscribeQueue(String subscribeQueue) {
        this.subscribeQueue = subscribeQueue;
    }

    public void setActive(AtomicBoolean active) {
        this.active = active;
    }

    public void setClientSocket(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public void initializeInputStream() {
        try {
            outputMessageStream = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader inputMessageReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            new ProducerReceiveProtocol(inputMessageReader, active).start();
        } catch (IOException exception) {
            LOGGER.error("The input stream cannot be created due to the following error ( " + exception.getMessage() + " )");
        }
    }
}

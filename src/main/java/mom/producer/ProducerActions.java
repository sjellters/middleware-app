package mom.producer;

import mom.consumer.Consumer;
import mom.message.Message;
import mom.queue.MessageQueue;
import mom.queue.QueueCreator;
import mom.queue.QueueMessageSender;
import mom.server.MiddlewareServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class ProducerActions {
    private final static Logger LOGGER = LogManager.getLogger(ProducerActions.class.getName());
    private final static Producer PRODUCER = MiddlewareServer.producer;
    private final static List<Consumer> CONSUMERS = MiddlewareServer.consumers;
    private static int currentConsumerIndex = 0;

    public static void onDisconnect(AtomicBoolean producerIsActive) {
        LOGGER.warn("Producer client just disconnected");
        producerIsActive.set(false);
    }

    public static void onDeclareQueue(String queueName, String type) {
        LOGGER.warn("A producer is trying to create a queue...");
        QueueCreator.declareQueue(queueName);

        if (type.equals("PUBLISH")) {
            PRODUCER.setPublishQueue(queueName);
        } else if (type.equals("SUBSCRIBE")) {
            PRODUCER.setSubscribeQueue(queueName);

            MessageQueue messageQueue = QueueCreator.messageQueueHashtable.get(queueName);
            for (String message : messageQueue.getMessages()) {
                PRODUCER.getMessageQueue().enqueueMessage(message);
            }

            LOGGER.info("Producer is subscribed to queue=" + queueName + " in mode=" + type);

            new QueueMessageSender(PRODUCER.getOutputMessageStream(), PRODUCER.getMessageQueue().getMessages()).start();
        }
    }

    public static void onReceiveMessage(Message parsedMessage) {
        LOGGER.info("Receiving message from Producer...");
        MessageQueue messageQueue = QueueCreator.messageQueueHashtable.get(parsedMessage.getQueueName());
        messageQueue.enqueueMessage(parsedMessage.getMessage());
        LOGGER.info("The message will be send to queue=" + parsedMessage.getQueueName());

        List<Consumer> consumerList = new ArrayList<>();

        for (Consumer consumer : CONSUMERS) {
            if (consumer.getSubscribeQueue().equals(parsedMessage.getQueueName())) {
//                consumer.getMessageQueue().enqueueMessage(parsedMessage.getMessage());
                consumerList.add(consumer);
            }
        }

        if (consumerList.size() == 2) {
            consumerList.get(currentConsumerIndex).getMessageQueue().enqueueMessage(parsedMessage.getMessage());

            if (currentConsumerIndex == 0) {
                currentConsumerIndex = 1;
            } else if (currentConsumerIndex == 1) {
                currentConsumerIndex = 0;
            }
        } else {
            for (Consumer consumer : consumerList) {
                consumer.getMessageQueue().enqueueMessage(parsedMessage.getMessage());
            }
        }
    }
}

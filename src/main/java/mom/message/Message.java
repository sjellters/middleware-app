package mom.message;

import java.util.Arrays;
import java.util.List;

public class Message {
    private String action;
    private String message;
    private String queueName;

    public Message() {
    }

    public static Message parse(String message) {
        List<String> attributes = Arrays.asList(message.split("/"));
        Message messageAsObject = new Message();

        messageAsObject.setAction(attributes.get(0));

        if (attributes.size() == 3) {
            messageAsObject.setMessage(attributes.get(1));
            messageAsObject.setQueueName(attributes.get(2));
        }

        return messageAsObject;
    }

    public String getRawMessage() {
        return String.format("%s/%s/%s", action, message, queueName);
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }
}

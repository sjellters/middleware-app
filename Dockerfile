FROM openjdk:11
COPY target/MiddlewareApplication.jar /usr/src/app/
WORKDIR /usr/src/app
EXPOSE 3000
EXPOSE 4000
CMD ["java","-jar","MiddlewareApplication.jar"]